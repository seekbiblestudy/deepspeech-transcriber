cp DeepSpeech.yaml DeepSpeech-local.yaml
sed -i "s/<VERSION>/${CI_COMMIT_SHA}/g" DeepSpeech-local.yaml
kubectl --namespace ${KUBE_NAMESPACE} apply -f DeepSpeech-local.yaml
rm DeepSpeech-local.yaml
