FROM python:3.8

ENV POETRY_VERSION=1.0.5

RUN apt update && apt install -y ffmpeg sox && apt clean

RUN pip install poetry==${POETRY_VERSION}

COPY pyproject.toml poetry.lock ./
RUN poetry install --no-dev --no-root

COPY . .
RUN poetry install --no-dev

CMD ["poetry", "run", "python", "-m", "deepspeech_transcriber"]
