import logging
import os
from dataclasses import dataclass
from typing import List, Literal, Optional
from pathlib import Path

import requests

logger = logging.getLogger(__name__)

version = "0.2"


@dataclass
class Job:
    recording_id: str
    id: str


class Jobs:
    """Interface to Seek's transcription jobs API."""

    def __init__(
        self, api_base: str, name: str = "DeepSpeech Transcriber " + version
    ) -> None:
        logger.debug("Initializing Jobs API")
        self.api_base = api_base
        authorization = os.environ.get("AUTHORIZATION_KEY")
        if not authorization:
            raise MissingAuthorizationKey(
                "Environment variable 'AUTHORIZATION_KEY' is not set"
            )

        headers = {"Authorization": authorization}

        self.client = requests.Session()
        self.client.headers.update(headers)
        self.name = name
        self.register_transcriber()

    def register_transcriber(self) -> None:
        """Send the name of this client to register with the API."""
        logger.info(f"Registering transcriber: {self.name}")
        body = {"name": self.name}
        response = self.client.post(
            f"{self.api_base}/transcriptions/transcribers", json=body
        )
        response.raise_for_status()

    def get_jobs(self, filter_by: Literal["todo", "processing"] = "todo") -> List[Job]:
        """Get a list of transcription jobs from the API."""
        logger.info("Getting list of jobs")
        parameters = {"todo": "true", "transcriber": self.name}
        response = self.client.get(f"{self.api_base}/transcriptions", params=parameters)
        response.raise_for_status()
        response_json = response.json()
        logger.debug(f"Response: {response_json}")
        return [
            Job(recording_id=job_data["recordingId"], id=job_data["id"])
            for job_data in response_json
        ]

    def get_job_url(self, job: Job) -> str:
        logger.info(f"Getting file url for job {job.id}")
        response = self.client.get(f"{self.api_base}/recordings/{job.recording_id}/url")
        response.raise_for_status()
        response_json = response.json()
        logger.debug(f"Got job url from API: {response_json['url']}")
        return response_json["url"]

    def process_job(self, job: Job) -> bool:
        logger.info(f"Telling API that we're about to start processing {job.id}")
        response = self.client.put(f"{self.api_base}/transcriptions/{job.id}/process")
        response.raise_for_status()
        response_json = response.json()
        return response_json["alreadyProcessing"]

    def download_recording(self, job: Job, url: str) -> bytes:
        logger.info(f"Downloading recording")
        response = requests.get(url)
        response.raise_for_status()
        return response.content

    def finish_job(self, job: Job, transcript: str, metadata: str = "{}") -> None:
        logger.info(f"Sending transcript for job {job.id}")
        body = {"transcriptText": transcript, "transcriptMetadata": metadata}
        response = self.client.put(
            f"{self.api_base}/transcriptions/{job.id}/finish", json=body
        )
        response.raise_for_status()


class MissingAuthorizationKey(Exception):
    """Raised when the authorization key was not found in the environment"""
