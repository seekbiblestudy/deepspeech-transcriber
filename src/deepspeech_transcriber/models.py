import logging
import tarfile
from io import BytesIO
from pathlib import Path
from importlib import metadata
from typing import List

import requests
from tqdm import tqdm

logger = logging.getLogger(__name__)


def download_models(path, version: str = metadata.version("deepspeech")):
    url = f"https://github.com/mozilla/DeepSpeech/releases/download/v{version}/deepspeech-{version}-models.tar.gz"

    data = bytearray()
    stream = requests.get(url)
    progress = tqdm(
        total=int(stream.headers["Content-Length"]),
        unit="B",
        unit_scale=True,
        desc="Downloading models",
    )

    for chunk in stream.iter_content(chunk_size=None):
        data.extend(chunk)
        progress.update(len(chunk))

    progress.close()
    logger.info("Extracting files from archive")
    archive = tarfile.open(fileobj=BytesIO(data))

    members = archive.getmembers()
    wanted_members = (".pbmm", "lm", "trie")
    filtered_members: List[tarfile.TarInfo] = []
    for member in members:
        for wanted in wanted_members:
            if wanted in member.name:
                filtered_members.append(member)

    path.mkdir(exist_ok=True, parents=True)
    logger.info(f"Extracting to {path}")
    archive.extractall(path, filtered_members)
    inner_directory = list(path.glob("deepspeech*"))[0]
    for file in inner_directory.iterdir():
        file.rename(file.absolute().parents[1] / file.name)
