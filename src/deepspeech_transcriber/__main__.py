from io import BytesIO
import logging
import subprocess
import os
from pathlib import Path
import typing

import lecture_transcriber
import lecture_transcriber.transcribe
import lecture_transcriber.utils
from lecture_transcriber import preprocess_audio
import requests

from . import api
from . import models


logging.getLogger().setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)

MODELS_PATH = Path("./models/models")


def process_jobs() -> None:
    logger.debug("Processing jobs")
    api_base = os.environ["API_ENDPOINT"]
    logger.debug(f"{api_base=}")
    jobs_api = api.Jobs(api_base)

    jobs = jobs_api.get_jobs()
    if jobs and not MODELS_PATH.exists():
        models.download_models(MODELS_PATH)
    count = 0
    process_args = None
    for job in jobs:
        logger.info(f"Preparing to process job: {job.id}")
        is_running = jobs_api.process_job(job)
        if is_running:
            logger.info(f"Someone else is already processing {job.id}, continuing...")
            continue

        url = jobs_api.get_job_url(job)
        try:
            audio_data = jobs_api.download_recording(job, url)
        except requests.exceptions.HTTPError as error:
            logger.error(
                f"Received HTTPError: {error.response.status_code} from url {error.request.url}"
            )
            logger.error(f"Content of the error: {error.response.content}")
            logger.info("Continuing to next job...")
            continue

        logger.info("Opening model")
        model = lecture_transcriber.utils.create_deepspeech_model(
            model=str(MODELS_PATH / "output_graph.pbmm"),
            trie=str(MODELS_PATH / "trie"),
            lm=str(MODELS_PATH / "lm.binary"),
        )
        logger.info("Creating audiosegment")
        audio_data, _ = preprocess_audio.get_audiosegment(
            BytesIO(audio_data), model.sampleRate()
        )
        logger.info(f"Beginning transcription for job {job.id}")
        try:
            transcript = lecture_transcriber.transcribe.transcribe(audio_data, model)
            transcript = " ".join(transcript)
            count += 1
            jobs_api.finish_job(job, transcript)
        except Exception:
            logger.exception(f"{job.id} failed to transcribe, continuing...")

    logger.info(f"Completed {count} jobs")


if __name__ == "__main__":
    try:
        process_jobs()
    except Exception:
        logger.exception("Critical error, not able to continue jobs")
